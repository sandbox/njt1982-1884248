<?php

/**
 *  @file
 *  Create a YouTube Stream Wrapper class for the Media/Resource module.
 */

 /**
  *  Create an instance like this:
  *  $video = new MediaInternetWistiaHandler('wistia://?v=[video-code]');
  */

class MediaWistiaStreamWrapper extends MediaReadOnlyStreamWrapper {
  protected $base_url = 'http://home.wistia.com/medias/';

  function interpolateUrl() {
    return $this->base_url . $this->parameters['v'];
  }

  function getTarget($f) {
    return FALSE;
  }

  static function getMimeType($uri, $mapping = NULL) {
    return 'video/wistia';
  }

  function getOriginalThumbnailPath() {
    $parts = $this->get_parameters();

    $width=90;
    $height=60;
    $data = media_wistia_data($parts['v'], $width, $height);
    return isset($data['thumbnail_url']) ? $data['thumbnail_url'] : FALSE;
  }

  function getLocalThumbnailPath() {
    $parts = $this->get_parameters();
    $local_path = 'public://media-wistia/' . check_plain($parts['v']) . '.jpg';
    if (!file_exists($local_path)) {
      $dirname = drupal_dirname($local_path);
      file_prepare_directory($dirname, FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS);
      @copy($this->getOriginalThumbnailPath(), $local_path);
    }
    return $local_path;
  }
}
