<?php

/**
 * Implementation of MediaInternetBaseHandler.
 *
 * @see hook_media_internet_providers().
 */
class MediaInternetWistiaHandler extends MediaInternetBaseHandler {
  public function parse($embedCode) {
    $pattern = '@https?://(.+\.)?(wistia\.com|wi\.st)/((m|medias|projects)|embed/(iframe|playlists))/([a-zA-Z0-9]+)@';
    $matches = array();
    if (preg_match($pattern, $embedCode, $matches)) {
      return file_stream_wrapper_uri_normalize('wistia://v/' . $matches[6]);
    }
  }

  public function valid_id($id) {
    if (!media_wistia_data($id)) {
      throw new MediaInternetValidationException(t('The Wistia video ID is invalid or the video was deleted.'));
    }
    return TRUE;
  }

  public function claim($embedCode) {
    if ($this->parse($embedCode)) {
      return TRUE;
    }
  }

  public function getFileObject() {
    $uri = $this->parse($this->embedCode);
    $file = file_uri_to_object($uri, TRUE);

    //if (empty($file->fid) && $info = $this->getOEmbed()) {
    //  $file->filename = truncate_utf8($info['title'], 255);
    //}

    return $file;
  }

  /**
   * Returns information about the media. See http://www.oembed.com/.
   *
   * @return
   *   If oEmbed information is available, an array containing 'title', 'type',
   *   'url', and other information as specified by the oEmbed standard.
   *   Otherwise, NULL.
   */
  // TODO...
  /*
  public function getOEmbed() {
    $uri = $this->parse($this->embedCode);
    $external_url = drupal_realpath($uri);
    $oembed_url = url('http://www.youtube.com/oembed', array('query' => array('url' => $external_url, 'format' => 'json')));
    $response = drupal_http_request($oembed_url);
    if (!isset($response->error)) {
      return drupal_json_decode($response->data);
    }
  }
  */
}
