<?php

/**
 * @file
 * Provide support for the Wistia provider to the emfield.module.
 *
 * @todo Implement EMMODULE_PROVIDER_duration() once supported by the Wistia
 *   API.
 */

/**
 * The main URL for Wistia.
 */
define('MEDIA_WISTIA_PROVIDER_URL', 'http://www.wistia.com/');

/**
 * The version of the serialized content data array.
 */
define('MEDIA_WISTIA_DATA_VERSION', 1);

/**
 * Implements EMMODULE_PROVIDER_info().
 *
 * @todo Add additional capabilities indicated by the API?
 */
function emvideo_wistia_info() {
  return array(
    'provider' => 'wistia',
    'name' => t('Wistia'),
    'url' => MEDIA_WISTIA_PROVIDER_URL,
    'settings_description' => t('These settings specifically affect videos displayed from <a href="@url">Wistia</a>.', array(
      '@url' => MEDIA_WISTIA_PROVIDER_URL,
    )),
    'supported_features' => array(
      array(t('Thumbnails'), t('Yes'), ''),
    ),
  );
}

/**
 * Implements EMMODULE_PROVIDER_data().
 */
function emvideo_wistia_data($field, $item) {
  $video_code = $item['value'];
  $width = $field['widget']['video_width'];
  $height = $field['widget']['video_height'];
  $data = media_wistia_data($video_code, $width, $height);
  return $data;
}

/**
 * Implements EMMODULE_PROVIDER_data_version().
 */
function emvideo_wistia_data_version() {
  return MEDIA_WISTIA_DATA_VERSION;
}

/**
 * Implements EMMODULE_PROVIDER_extract().
 */
function emvideo_wistia_extract($embed, $field) {
  $pattern = '@https?://(.+\.)?(wistia\.com|wi\.st)/((m|medias|projects)|embed/(iframe|playlists))/([a-zA-Z0-9]+)@';
  $matches = array();
  if (preg_match($pattern, $embed, $matches)) {
    return $matches[6];
  }
}

/**
 * Implements EMMODULE_PROVIDER_validate().
 */
function emvideo_wistia_validate($code, $error_field) {
  if (!media_wistia_data($code)) {
    form_set_error($error_field, t('There is no such video on wistia.com.'));
  }
}

/**
 * Implements EMMODULE_PROVIDER_preview().
 */
function emvideo_wistia_preview($video_code, $width, $height, $field, $item, $node, $autoplay, $options = array()) {
  return emvideo_wistia_video($video_code, $width, $height, $field, $item, $node, $autoplay, $options);
}

/**
 * Implements EMMODULE_PROVIDER_video().
 */
function emvideo_wistia_video($video_code, $width, $height, $field, $item, $node, $autoplay, $options = array()) {
  return theme('media_wistia_embed', $video_code, $width, $height, $item);
}

/**
 * Implements EMMODULE_PROVIDER_thumbnail().
 */
function emvideo_wistia_thumbnail($field, $item, $formatter, $node, $width, $height) {
  $video_code = $item['value'];
  $data = media_wistia_data($video_code, $width, $height);
  if (isset($data['thumbnail_url'])) {
    // Strip image crop parameter to get full-sized original.
    $old_image_crop_resized = 'image_crop_resized=' . $data['thumbnail_width'] . 'x' . $data['thumbnail_height'];
    $new_image_crop_resized = 'image_crop_resized=' . $width . 'x' . $height;
    $url = str_replace($old_image_crop_resized, $new_image_crop_resized, $data['thumbnail_url']);
    return $url;
  }
}

/**
 * Implements EMMODULE_PROVIDER_embedded_link().
 */
function emvideo_wistia_embedded_link($code, $data = array()) {
  $pattern = '/src="(.*)"/U';
  $matches = array();
  preg_match($pattern, $data['html'], $matches);
  $url = preg_replace('/\?.*/', '', $matches[1]);
  return $url;
}

/**
 *  Implements EMMODULE_PROVIDER_content_generate().
 */
function emvideo_wistia_content_generate() {
  return array(
    'http://wistia.com/projects/yc9r461ryx',
    'http://wistia.com/medias/9pj9n6ftlk',
    'http://wistia.com/medias/bq84enk6m3',
    'http://wistia.com/medias/e2hzefsyfy',
    'http://wistia.com/medias/4xxqj2lpjy',
    'http://wistia.com/medias/twldzxm17h',
    'http://wistia.com/medias/x3n6gdvb2y',
  );
}
