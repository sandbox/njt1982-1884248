
CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation
 * Upgrading to 2.x
 * Usage
 * Known Issues


INTRODUCTION
------------

This module adds support for Wistia (http://wistia.com/) as a video content
provider for Embedded Video Field, part of the Embedded Media Field suite of
modules (http://drupal.org/project/emfield). It supports public and private
videos and playlists using either their Wistia.com URLs or complete embed code.


INSTALLATION
------------

Media: Wistia is installed in the usual way. See
http://drupal.org/documentation/install/modules-themes/modules-5-6. It depends
on Embedded Video Field, part of the Embedded Media Field suite of
modules (http://drupal.org/project/emfield), which in turn depends on CCK
(http://drupal.org/project/cck).


UPGRADING TO 2.x
----------------
The 2.x branch of Media: Wistia has been completely rewritten to use oEmbed
instead of the Wistia Data API. Your API key is therefore no longer necessary
and will be removed from the variables table when you upgrade. The module now
supports both public and private videos, playlists, and Embed Builder customized
players. It also validates URls and embed code to see if the video or playlist
they refer to actually exist on Wistia, and it caches video data to speed page
loads.


USAGE
-----

1. To use the module, add to a given content type an "Embedded Video" field
   using the "3rd Party Video" widget.
2. On the field settings screen, enable the "Wistia" provider and configure the
   settings.
3. Now embed videos and playlists by pasting into the new field either the
   Wistia URL (for a generic player) or embed code from Wistia's embed builders
   (for a customized player). Here are some (inexhaustive) examples:

   Videos:
   * http://username.wistia.com/medias/12345
   * http://wistia.com/medias/12345
   * http://wi.st/m/12345
   * Complete embed code from the SuperEmbed Builder.

   Playlists:
   * http://username.wistia.com/projects/12345
   * Complete embed code from the Playlist Builder.


KNOWN ISSUES
============

* Video data is cached. If you make changes to a video on Wistia.com, you may
  need to clear the Drupal cache for them to take effect on your site.
* Video height and width as set in Wistia's embed builders are intentionally
  ignored, and the height and width set in the field settings are used instead.
* Thumbnails are not _truly_ resized. See http://drupal.org/node/1052444.
